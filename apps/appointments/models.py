from __future__ import unicode_literals
from datetime import datetime
from django.utils import timezone

from django.db import models

from ..users.models import User

# Create your models here.

STATUS_TYPE_CHOICES = (
    ('0', 'Pending'),
    ('1', 'Done'),
    ('2', 'Missed'),
)

PENDING = '0'
DONE = '1'
MISSED = '2'

class AppointmentManager(models.Manager):
    def validate(self, form, u_id):
        data = {}
        errors = {}
        flag = False
        for key, val in form.iteritems():
            if key != 'csrfmiddlewaretoken':
                data[key] = str(val)

        if len(data['task']) == 0:
            errors['task'] = 'Please write a task'


        # Appointment Checks
		# Collect error prone values from kwargs and validate

        try:
            print data['time']
            task_time = datetime.strptime(data['time'], '%H:%M')

        except Exception as e:
            flag = True
            errors['time'] = "Please enter a value for time"

        try:
            print data['date']
            task_date = datetime.strptime(data['date'], '%Y-%m-%d')

        except Exception as e:
            flag = True
            errors['date'] = "Please enter a value for 'Date of Birth'"

        try:
            delta = datetime.now() - task_date
            if delta.days > 0:
                print delta
                flag = True
                errors['date'] = "I'm sorry, you may only select days today and later"
        except Exception as e:
            pass

        if flag:
            return (False, errors)
        try:
            data['user']=User.manager.get(id=u_id)
            appointment = Appointment.objects.create(**data)
        except Exception as e:
            errors['database_error'] = e
            return (False, errors)

        return (True, appointment)

class Appointment(models.Model):
    date = models.DateField()
    time = models.TimeField()
    task = models.TextField()
    status = models.CharField(max_length=1, choices=STATUS_TYPE_CHOICES, default=PENDING)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, related_name="appointment")
    objects = models.Manager()
    manager = AppointmentManager()

