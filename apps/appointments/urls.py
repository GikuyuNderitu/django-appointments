from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name="index"),
    url(r'^create$', create, name="create"),
    url(r'^(?P<id>\d+)$', edit, name="edit"),
    url(r'^(?P<id>\d+)/delete$', delete, name="delete"),
]