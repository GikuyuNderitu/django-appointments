from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.urlresolvers import reverse

from datetime import datetime
from ..users.models import User
from .models import Appointment


# Create your views here.
def index(request):
    if 'id' not in request.session:
        return redirect(reverse('entry:index'))
    now = datetime.now()
    data ={
        'user': User.manager.get(id=request.session['id']),
        'now': now,
        'todays_apps': Appointment.objects.filter(date=now.date()),
        'future_apps': Appointment.objects.filter(date__gt=now.date()),
    }
    return render(request, 'appointments/index.html', data)

def create(request):
    result = Appointment.manager.validate(form=request.POST, u_id=request.session['id'])

    if not result[0]:
        for key, val in result[1].iteritems():
            messages.error(request, val)
    return redirect(reverse('appointments:index'))

def edit(request, id):
    if request.method == 'GET':
        data = {
            'app': Appointment.objects.get(id=id)
        }
        return render(request, 'appointments/edit.html', data)
    return redirect(reverse('appointments:index'))

def delete(request):
    return redirect(reverse('appointments:index'))
