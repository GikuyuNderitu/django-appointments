from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', redirect_index),
    url(r'^main$', index, name="index"),
]