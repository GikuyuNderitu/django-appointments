from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.urlresolvers import reverse


# Create your views here.
def index(request):
    if 'id' in request.session:
        return redirect(reverse('appointments:index'))
    return render(request, 'entry/index.html')

def redirect_index(request):
    return redirect(reverse('entry:index'))