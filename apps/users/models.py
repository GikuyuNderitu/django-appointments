from __future__ import unicode_literals
from datetime import datetime
from django.utils import timezone
import re
import bcrypt


from django.db import models

EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')

# Create your models here.
class UserManager(models.Manager):
    def register(self, **kwargs):
        data = {}
        flag = False
        errors = {}
        for key, value in kwargs['kwargs'].iteritems():
            if key != 'csrfmiddlewaretoken':
                data[key] = str(kwargs['kwargs'][key])
        
        if len(data['name']) == 0:
            errors['name'] = 'Name is required'
            flag = True
        
        if len(data['email']) == 0:
            errors['email'] = 'Email is required'
            flag = True

        elif not EMAIL_REGEX.match(data['email']):
            errors['email'] = 'Please enter a valid email'
            flag = True

        if len(data['password']) == 0:
            errors['password'] = 'Password is required'
            flag = True

        elif len(data['password']) < 8:
            errors['password'] = 'Password needs to be 8 or more characters'
            flag = True   

        if data['password'] != data['password_confirmation']:
            errors['password_confirmation'] = 'Password and ConfirmPW need to match'
            flag = True

        # DOB Checks
		# Collect error prone values from kwargs and validate
        try:
            print data['birthday']
            dob = datetime.strptime(data['birthday'], '%Y-%m-%d')

        except Exception as e:
            flag = True
            errors['birthday'] = "Please enter a value for 'Date of Birth'"

        try:
            delta = datetime.now() - dob
            if delta.days <= 0:
                flag = True
                errors['birthday'] = "I'm sorry, you may only select any day prior to today"
        except Exception as e:
            pass

        # Check to see if flag was tripped at all
        if flag:
            print errors
            return (False, errors)

        hashed = bcrypt.hashpw(data['password'], bcrypt.gensalt(14))
        data['password'] = hashed
        del data['password_confirmation']
        data['date_of_birth'] = timezone.make_aware(dob)
        del data['birthday']

        try:
            user = User.manager.create(**data)
        except Exception as e:
            print e
            print 'error creating user'
            errors['email'] = "I'm sorry, there is already a user with that email."
            return (False, errors)

        return (True, user)

    def login(self, **kwargs):
        errors = {}
        data = {}
        for key, value in kwargs['kwargs'].iteritems():
            if key != 'csrfmiddlewaretoken':
                data[key] = str(kwargs['kwargs'][key])
        
        flag = True

        if len(data['email']) == 0:
            flag = False
            errors['email'] = 'The email field is required'
        elif not EMAIL_REGEX.match(data['email']):
            flag = False
            errors['login_email'] = 'Please enter a valid email'
        
        try:
            user = User.manager.get(email=data['email'])
        except Exception as e:
            errors['email'] = "I'm sorry, that email doesn't exist"

        if user.password != bcrypt.hashpw(data['password'], user.password.encode()):
            errors['password'] = "Password is incorrect"
            flag = False

        if not flag:
            return (flag, errors)
        
        return (flag, user)

class User(models.Model):
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255, unique=True)
    password = models.CharField(max_length=255)
    date_of_birth = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    manager = UserManager()

    def __repr__(self):
        return "Name: {}\nEmail: {}\nPassword: \nDate_of_Birth: {}\n".format(self.name, self.email, self.password, self.date_of_birth)