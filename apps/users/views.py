from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.urlresolvers import reverse

from .models import User

# Create your views here.
def login(request):
    result = User.manager.login(kwargs=request.POST)
    if not result[0]:
        for key, val in result[1].iteritems():
            messages.error(request, val)
        return redirect(reverse('entry:index'))

    request.session['id'] = result[1].id
    return redirect(reverse('appointments:index'))

def create(request):
    result = User.manager.register(kwargs=request.POST)
    if not result[0]:
        print result[1]
        for key, val in result[1].iteritems():
            messages.error(request, val)
        return redirect(reverse('entry:index'))

    print result[1]
    request.session['id'] = result[1].id
    return redirect(reverse('appointments:index'))

def logout(request):
    request.session.flush()
    return redirect(reverse('entry:index'))
